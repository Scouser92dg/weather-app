package czajkowski.patryk.weatherapp.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import czajkowski.patryk.weatherapp.model.Weather
import io.reactivex.Maybe
import org.threeten.bp.ZonedDateTime

@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWeather(weather: Weather)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWeathers(weathers: List<Weather>)

    @Query("SELECT * from Weather where cityKey = :cityKey LIMIT 1")
    fun getWeatherByCityKey(cityKey: String): Maybe<Weather>

    @Query("SELECT * from Weather where cityKey = :cityKey AND date BETWEEN :dateStarted AND :dateAfter LIMIT 5")
    fun getNextDaysWeathers(
        cityKey: String,
        dateStarted: ZonedDateTime = ZonedDateTime.now(),
        dateAfter: ZonedDateTime = ZonedDateTime.now().plusDays(5)
    ): Maybe<List<Weather>>

}