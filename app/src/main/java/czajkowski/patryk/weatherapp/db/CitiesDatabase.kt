package czajkowski.patryk.weatherapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import czajkowski.patryk.weatherapp.model.City


@Database(entities = [City::class], version = CitiesDatabase.VERSION)
abstract class CitiesDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "cities.db"
        const val VERSION = 4

        @Volatile
        private var INSTANCE: CitiesDatabase? = null

        fun getInstance(context: Context): CitiesDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: bindDatabase(context).also { INSTANCE = it }
            }

        private fun bindDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, CitiesDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

    abstract fun cityDao(): CityDao
}