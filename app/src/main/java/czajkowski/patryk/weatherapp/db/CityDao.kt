package czajkowski.patryk.weatherapp.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import czajkowski.patryk.weatherapp.model.City
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface CityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCity(city: City)

    @Query("SELECT * from City")
    fun getAllCities(): Single<List<City>>

    @Query("SELECT * from City where cityKey = :cityKey LIMIT 1")
    fun getCityByKey(cityKey: String): Maybe<City>

}