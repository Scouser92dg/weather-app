package czajkowski.patryk.weatherapp.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import czajkowski.patryk.weatherapp.model.Weather
import czajkowski.patryk.weatherapp.util.ZonedDateTimeConverter


@Database(entities = [Weather::class], version = WeathersDatabase.VERSION)
@TypeConverters(ZonedDateTimeConverter::class)
abstract class WeathersDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "weathers.db"
        const val VERSION = 7

        @Volatile
        private var INSTANCE: WeathersDatabase? = null

        fun getInstance(context: Context): WeathersDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: bindDatabase(context).also { INSTANCE = it }
            }

        private fun bindDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, WeathersDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

    abstract fun weatherDao(): WeatherDao

}