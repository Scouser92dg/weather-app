package czajkowski.patryk.weatherapp.db

import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val daoModule = module {
    single<WeatherDao> { WeatherDao_Impl(WeathersDatabase.getInstance(androidApplication())) }
    single<CityDao> { CityDao_Impl(CitiesDatabase.getInstance(androidApplication())) }
}