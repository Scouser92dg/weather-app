package czajkowski.patryk.weatherapp.ui.details

import androidx.lifecycle.MutableLiveData
import czajkowski.patryk.weatherapp.ui.base.BaseViewModel
import czajkowski.patryk.weatherapp.repository.CityRepository
import czajkowski.patryk.weatherapp.repository.WeatherRepository
import czajkowski.patryk.weatherapp.ui.model.CityModelWrapper
import czajkowski.patryk.weatherapp.ui.model.WeatherModelWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class DetailsViewModel(private val weatherRepository: WeatherRepository, private val cityRepository: CityRepository): BaseViewModel() {

    val city = MutableLiveData<CityModelWrapper>()
    val errorMessage = MutableLiveData<String>()
    val weatherToday = MutableLiveData<WeatherModelWrapper>()
    val weathersNextDays = MutableLiveData<List<WeatherModelWrapper>>()

    fun fetchDetails(cityKey: String) {
        fetchCity(cityKey)
        fetchTodayWeather(cityKey)
        fetchNextDaysWeathers(cityKey)
    }

    private fun fetchTodayWeather(cityKey: String) {
        weatherRepository.getDailyWeather(cityKey)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onError = {
                    it.localizedMessage?.let {
                        errorMessage.postValue(it)
                    }
                },
                onNext = { weather ->
                    weatherToday.postValue(WeatherModelWrapper(weather))
                    weatherRepository.storeWeatherInDb(weather)
                        .subscribeBy(
                            onError = {
                                it.localizedMessage?.let {
                                    errorMessage.postValue(it)
                                }
                            }
                        )
                }
            ).addTo(disposables)
    }

    private fun fetchCity(cityKey: String) {
        cityRepository.getCity(cityKey)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onError = {
                    it.localizedMessage?.let {
                        errorMessage.postValue(it)
                    }
                },
                onNext = {
                    city.postValue(CityModelWrapper(it))
                    cityRepository.storeCityInDb(it)
                        .subscribeBy(
                            onError = {
                                it.localizedMessage?.let {
                                    errorMessage.postValue(it)
                                }
                            }
                        ).addTo(disposables)
                }
            ).addTo(disposables)
    }

    private fun fetchNextDaysWeathers(cityKey: String) {
        weatherRepository.getNextDaysWeathers(cityKey)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(400, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onError = {
                    it.localizedMessage?.let {
                        errorMessage.postValue(it)
                    }
                },
                onNext = {
                    weatherRepository.storeWeathersInDb(it)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeBy(
                            onError = {
                                it.localizedMessage?.let {
                                    errorMessage.postValue(it)
                                }
                            },
                            onComplete = {
                                weathersNextDays.postValue(it.map { weather -> WeatherModelWrapper(weather) })
                            }
                        )
                }
            ).addTo(disposables)
    }

}