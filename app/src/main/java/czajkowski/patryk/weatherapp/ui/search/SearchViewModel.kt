package czajkowski.patryk.weatherapp.ui.search

import androidx.lifecycle.MutableLiveData
import czajkowski.patryk.weatherapp.ui.base.BaseViewModel
import czajkowski.patryk.weatherapp.repository.CityRepository
import czajkowski.patryk.weatherapp.ui.model.CityModelWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class SearchViewModel(private val cityRepository: CityRepository) : BaseViewModel() {

    val errorMessage = MutableLiveData<String>()
    val cities = MutableLiveData<List<CityModelWrapper>>()
    val isLastCitiesInfoHidden = MutableLiveData<Boolean>()
    val isLastCitiesExists = MutableLiveData<Boolean>()

    init {
        cityRepository.getCitiesFromDb()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = {
                    it.localizedMessage?.let {
                        errorMessage.postValue(it)
                    }
                },
                onNext = {
                    if (it.isNotEmpty()) {
                        isLastCitiesExists.postValue(true)
                    }
                    cities.postValue(it.map { CityModelWrapper(it) })
                }
            ).addTo(disposables)
    }

    fun searchCities(text: String) {
        if (text.toCharArray().size >= 2) {
            cityRepository.searchCities(text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onError = {
                        it.localizedMessage?.let {
                            errorMessage.postValue(it)
                        }
                    },
                    onNext = {
                        if (it.isNotEmpty()) {
                            isLastCitiesInfoHidden.postValue(true)
                            cities.postValue(it.map { CityModelWrapper(it) })
                        }
                    }
                ).addTo(disposables)
        } else {
            cities.postValue(listOf())
        }
    }

}