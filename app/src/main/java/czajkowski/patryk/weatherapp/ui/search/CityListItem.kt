package czajkowski.patryk.weatherapp.ui.search

import android.os.Bundle
import android.view.View
import com.mikepenz.fastadapter.diff.DiffCallback
import czajkowski.patryk.weatherapp.ui.model.CityModelWrapper
import czajkowski.patryk.weatherapp.util.SimpleAbstractItem
import kotlinx.android.synthetic.main.list_item_city.view.*

class CityListItem(val city: CityModelWrapper, private val actionClick: (key: String) -> Unit): SimpleAbstractItem() {

    override fun bind(view: View) {
        view.cityListItemNameText.text = city.englishName
        view.cityListItemCountryText.text = city.countryEnglishName
        view.setOnClickListener {
            actionClick(city.cityKey)
        }
    }

    override val layoutRes: Int = czajkowski.patryk.weatherapp.R.layout.list_item_city

    class CityDiffCallback : DiffCallback<CityListItem> {
        override fun areItemsTheSame(oldItem: CityListItem, newItem: CityListItem): Boolean {
            return true
        }

        override fun areContentsTheSame(oldItem: CityListItem, newItem: CityListItem): Boolean {
            return newItem.city.cityKey == oldItem.city.cityKey
        }

        override fun getChangePayload(
            oldItem: CityListItem,
            oldItemPosition: Int,
            newItem: CityListItem,
            newItemPosition: Int
        ): Any? {
            val diff = Bundle()
            if (newItem.city.cityKey != oldItem.city.cityKey) {
                diff.putString("key", newItem.city.cityKey)
            }

            if (diff.size() == 0) {
                return null
            }
            return diff
        }
    }

}