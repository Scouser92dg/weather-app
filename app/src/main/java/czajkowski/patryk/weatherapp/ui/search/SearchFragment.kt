package czajkowski.patryk.weatherapp.ui.search

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import czajkowski.patryk.weatherapp.R
import czajkowski.patryk.weatherapp.ui.details.DetailsFragment
import czajkowski.patryk.weatherapp.ui.model.CityModelWrapper
import kotlinx.android.synthetic.main.fragment_search.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class SearchFragment: Fragment() {

    private val citiesAdapter = FastItemAdapter<CityListItem>()

    private val viewModel: SearchViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        citiesRecycler.adapter = citiesAdapter
        citiesRecycler.addItemDecoration(DividerItemDecoration(citiesRecycler.getContext(), LinearLayoutManager.VERTICAL))

        viewModel.cities.observe(viewLifecycleOwner, Observer {
            setItemsOnAdapter(it, citiesAdapter)
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            Snackbar.make(searchFragmentLayout, it, Snackbar.LENGTH_LONG).show()
        })

        viewModel.isLastCitiesInfoHidden.observe(viewLifecycleOwner, Observer {
            searchLastCitiesInfo.isVisible = !it
        })

        viewModel.isLastCitiesExists.observe(viewLifecycleOwner, Observer {
            searchLastCitiesInfo.text = if (it) {
                getString(R.string.search_last_cities_info_label)
            } else {
                getString(R.string.search_no_cities_info)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.searchMenuItem -> {
                initSearchView(item)
            }
        }
        return true
    }

    private fun initSearchView(item: MenuItem) {
        val searchView: SearchView = item.getActionView() as SearchView
        searchView.setQueryHint(getString(R.string.search_hint))
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { newText ->
                    viewModel.searchCities(newText)
                }
                return true
            }
        })
    }

    private fun setItemsOnAdapter(items: List<CityModelWrapper>, adapter: FastItemAdapter<CityListItem>) {
        FastAdapterDiffUtil.set(
            adapter.itemAdapter,
            items.map {
                CityListItem(it) { cityKey ->
                    goToDetails(cityKey)
                    viewModel.isLastCitiesInfoHidden.postValue(false)
                    viewModel.isLastCitiesExists.postValue(true)
                }
            },
            CityListItem.CityDiffCallback()
        )
    }

    private fun goToDetails(cityKey: String) {
        activity?.findNavController(R.id.nav_host_fragment)?.let { navController ->
            if (navController.currentDestination?.id == R.id.searchFragment) {
                val bundle = Bundle()
                bundle.putString(DetailsFragment.BUNDLE_CITY_KEY, cityKey)
                navController.navigate(R.id.action_searchFragment_to_detailsFragment, bundle)
            }
        }
    }
}