package czajkowski.patryk.weatherapp.ui.model

import czajkowski.patryk.weatherapp.model.Weather
import org.threeten.bp.ZonedDateTime

data class WeatherModelWrapper(
    var cityKey: String,
    var date: ZonedDateTime,
    var temperatureMin: Double?,
    var temperatureMax: Double?,
    var dayRainIntensity: String = "None",
    var daySnowIntensity: String = "None",
    var dayIceIntensity: String = "None",
    var dayIconUrl: String,
    var dayDescription: String,
    var nightIconUrl: String,
    var nightDescription: String,
    var nightRainIntensity: String = "None",
    var nightSnowIntensity: String = "None",
    var nightIceIntensity: String = "None",
    var description: String,
    var category: String
) {
    constructor(weather: Weather) : this(
        weather.cityKey,
        weather.date,
        weather.temperatureMin,
        weather.temperatureMax,
        weather.dayRainIntensity,
        weather.daySnowIntensity,
        weather.dayIceIntensity,
        weather.dayIconUrl,
        weather.dayDescription,
        weather.nightIconUrl,
        weather.nightDescription,
        weather.nightRainIntensity,
        weather.nightSnowIntensity,
        weather.nightIceIntensity,
        weather.description,
        weather.category
    )
}

