package czajkowski.patryk.weatherapp.ui.model

import czajkowski.patryk.weatherapp.model.City


data class CityModelWrapper(
    var cityKey: String,
    var localizedName: String,
    var englishName: String,
    var countryLocalizedName: String,
    var countryEnglishName: String
) {
    constructor(city: City) : this(
        city.cityKey,
        city.localizedName,
        city.englishName,
        city.countryLocalizedName,
        city.countryEnglishName
    )
}