package czajkowski.patryk.weatherapp.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import czajkowski.patryk.weatherapp.R
import czajkowski.patryk.weatherapp.ui.model.WeatherModelWrapper
import czajkowski.patryk.weatherapp.util.setColorByTemperature
import czajkowski.patryk.weatherapp.util.setImageByUrl
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.weather_today_layout.*
import kotlinx.android.synthetic.main.weathers_next_days_layout.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailsFragment: Fragment() {

    private val nextDaysAdapter = FastItemAdapter<WeatherListItem>()

    private val viewModel: DetailsViewModel by viewModel()

    companion object {
        const val BUNDLE_CITY_KEY = "bundle_city_key"
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)?.supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)?.supportActionBar?.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getString(BUNDLE_CITY_KEY)?.let {
            viewModel.fetchDetails(it)
        }

        detailsWeatherNextDaysRecycler.adapter = nextDaysAdapter

        viewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            Snackbar.make(detailsFragmentLayout, it, Snackbar.LENGTH_LONG).show()
        })

        viewModel.weatherToday.observe(viewLifecycleOwner, Observer {
            detailsWeatherDayImage.setImageByUrl(requireContext(), it.dayIconUrl)
            detailsWeatherNightImage.setImageByUrl(requireContext(), it.nightIconUrl)
            detailsDayRainValue.text = it.dayRainIntensity
            detailsDaySnowValue.text = it.daySnowIntensity
            detailsDayIceValue.text = it.dayIceIntensity
            detailsNightRainValue.text = it.nightRainIntensity
            detailsNightSnowValue.text = it.nightSnowIntensity
            detailsNightIceValue.text = it.nightIceIntensity
            detailsDescriptionText.text = it.description
            it.temperatureMin?.let { temperature ->
                val temperatureMinString = getString(R.string.details_temperature_label, temperature)
                detailsTemperatureMinText.text = temperatureMinString
                detailsTemperatureMinText.setColorByTemperature(requireContext(), temperature)
            }
            it.temperatureMax?.let { temperature ->
                val temperatureMaxString = getString(R.string.details_temperature_label, temperature)
                detailsTemperatureMaxText.text = temperatureMaxString
                detailsTemperatureMaxText.setColorByTemperature(requireContext(), temperature)
            }

        })

        viewModel.city.observe(viewLifecycleOwner, Observer {
            detailsCityName.text = "${it.localizedName}, ${it.countryLocalizedName}"
        })

        viewModel.weathersNextDays.observe(viewLifecycleOwner, Observer {
            setItemsOnAdapter(it, nextDaysAdapter)
        })
    }

    private fun setItemsOnAdapter(items: List<WeatherModelWrapper>, adapter: FastItemAdapter<WeatherListItem>) {
        FastAdapterDiffUtil.set(
            adapter.itemAdapter,
            items.map {
                WeatherListItem(requireContext(), it)
            },
            WeatherListItem.WeatherDiffCallback()
        )
    }

}