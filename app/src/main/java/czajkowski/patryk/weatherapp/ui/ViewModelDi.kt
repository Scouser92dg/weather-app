package czajkowski.patryk.weatherapp.ui

import czajkowski.patryk.weatherapp.ui.details.DetailsViewModel
import czajkowski.patryk.weatherapp.ui.search.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { DetailsViewModel(get(), get()) }
    viewModel { SearchViewModel(get()) }
}