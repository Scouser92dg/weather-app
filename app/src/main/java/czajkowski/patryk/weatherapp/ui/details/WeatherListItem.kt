package czajkowski.patryk.weatherapp.ui.details

import android.content.Context
import android.os.Bundle
import android.view.View
import com.mikepenz.fastadapter.diff.DiffCallback
import czajkowski.patryk.weatherapp.R
import czajkowski.patryk.weatherapp.model.Weather
import czajkowski.patryk.weatherapp.ui.model.WeatherModelWrapper
import czajkowski.patryk.weatherapp.util.SimpleAbstractItem
import czajkowski.patryk.weatherapp.util.setColorByTemperature
import czajkowski.patryk.weatherapp.util.setImageByUrl
import kotlinx.android.synthetic.main.list_item_weather.view.*

class WeatherListItem(val context: Context, val weather: WeatherModelWrapper): SimpleAbstractItem() {

    override fun bind(view: View) {
        weather.temperatureMin?.let { temperature ->
            val temperatureMin = context.getString(R.string.details_temperature_label, temperature)
            view.weatherListItemTemperatureMinText.text = temperatureMin
            view.weatherListItemTemperatureMinText.setColorByTemperature(context, temperature)
        }
        weather.temperatureMax?.let { temperature ->
            val temperatureMax = context.getString(R.string.details_temperature_label, temperature)
            view.weatherListItemTemperatureMaxText.text = temperatureMax
            view.weatherListItemTemperatureMaxText.setColorByTemperature(context, temperature)
        }
        view.weatherListItemDate.text = "${weather.date.dayOfWeek.toString().take(3)} ${weather.date.dayOfMonth}/${weather.date.monthValue}"
        view.weatherListItemImage.setImageByUrl(context, weather.dayIconUrl)
    }

    override val layoutRes: Int = R.layout.list_item_weather

    class WeatherDiffCallback : DiffCallback<WeatherListItem> {
        override fun areItemsTheSame(oldItem: WeatherListItem, newItem: WeatherListItem): Boolean {
            return true
        }

        override fun areContentsTheSame(oldItem: WeatherListItem, newItem: WeatherListItem): Boolean {
            return newItem.weather.cityKey == oldItem.weather.cityKey
        }

        override fun getChangePayload(
            oldItem: WeatherListItem,
            oldItemPosition: Int,
            newItem: WeatherListItem,
            newItemPosition: Int
        ): Any? {
            val diff = Bundle()
            if (newItem.weather.cityKey != oldItem.weather.cityKey) {
                diff.putString("cityKey", newItem.weather.cityKey)
            }

            if (diff.size() == 0) {
                return null
            }
            return diff
        }
    }

}