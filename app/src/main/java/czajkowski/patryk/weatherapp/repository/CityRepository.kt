package czajkowski.patryk.weatherapp.repository

import android.util.Log
import czajkowski.patryk.weatherapp.api.WeatherApiService
import czajkowski.patryk.weatherapp.db.CityDao
import czajkowski.patryk.weatherapp.model.City
import czajkowski.patryk.weatherapp.model.CityResponse
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.*

class CityRepository(
    private val weatherApiService: WeatherApiService,
    private val cityDao: CityDao
) {

    fun getCitiesFromDb(): Observable<List<City>> {
        return cityDao.getAllCities()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .doOnNext {
                Log.d("WeatherRepository", "Dispatching ${it.size} cities from DB")
            }
    }

    fun searchCities(text: String): Observable<List<City>> {
        return weatherApiService.getCities(text)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .map { it.filter {
                    it.EnglishName.toLowerCase(Locale.ENGLISH) == text.toLowerCase(Locale.ENGLISH) ||
                    it.LocalizedName.toLowerCase(Locale.ENGLISH) == text.toLowerCase(Locale.ENGLISH)
                }
            }
            .map { it.distinctBy { it.LocalizedName } }
            .map {
                it.map {
                    mapResponseToCity(it)
                }
            }
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .doOnNext {
                Log.d("WeatherRepository", "Dispatchng ${it.size} cities from API")
            }
    }

    fun storeCityInDb(city: City): Completable {
        return Completable.fromCallable { cityDao.insertCity(city) }.subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
            .doOnComplete {
                Log.d("PlacesRepository", "Inserted city ${city.englishName} in DB")
            }
    }

    fun getCity(Key: String): Observable<City> {
        return Observable.concatArray(
            getCityFromDb(Key),
            getCityFromApi(Key)
        )
    }

    private fun getCityFromDb(key: String): Observable<City> {
        return cityDao.getCityByKey(key)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext(Maybe.empty())
            .toObservable()
            .doOnNext {
                Log.d("WeatherRepository", "Dispatchng city ${it.englishName} from DB")
            }

    }

    private fun getCityFromApi(cityKey: String): Observable<City> {
        return weatherApiService.getCity(cityKey)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .map {
                mapResponseToCity(it)
            }
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .doOnNext {
                Log.d("WeatherRepository", "Dispatchng city ${it.englishName} from API")
            }
    }

    private fun mapResponseToCity(it: CityResponse): City {
        return City(
            localizedName = it.LocalizedName,
            englishName = it.EnglishName,
            cityKey = it.Key,
            countryEnglishName = it.Country.EnglishName,
            countryLocalizedName = it.Country.LocalizedName
        )
    }

}