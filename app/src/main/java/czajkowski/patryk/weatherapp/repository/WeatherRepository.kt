package czajkowski.patryk.weatherapp.repository

import android.util.Log
import czajkowski.patryk.weatherapp.api.WeatherApiService
import czajkowski.patryk.weatherapp.db.WeatherDao
import czajkowski.patryk.weatherapp.model.Weather
import czajkowski.patryk.weatherapp.model.WeatherResponse
import czajkowski.patryk.weatherapp.util.getCelsiusValue
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.ZonedDateTime

class WeatherRepository(
    private val weatherApiService: WeatherApiService,
    private val weatherDao: WeatherDao
) {

    companion object {
        const val ICONS_URL =
            "https://apidev.accuweather.com/developers/Media/Default/WeatherIcons/"
        const val RAIN = "Rain"
        const val SNOW = "Snow"
        const val ICE = "Ice"
        const val MIXED = "Mixed"
    }

    fun getDailyWeather(cityKey: String): Observable<Weather> {
        return Observable.concatArray(
            getDailyWeatherFromDb(cityKey),
            getDailyWeatherFromApi(cityKey)
        )
    }

    private fun getDailyWeatherFromDb(cityKey: String): Observable<Weather> {
        return weatherDao.getWeatherByCityKey(cityKey)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext(Maybe.empty())
            .toObservable()
            .doOnNext {
                Log.d("WeatherRepository", "Dispatching Weather with id: $cityKey from DB...")
            }
    }

    private fun getDailyWeatherFromApi(cityKey: String): Observable<Weather> {
        return weatherApiService.getWeatherDaily(cityKey)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .map { weatherResponse ->
                mapResponseToWeather(weatherResponse, cityKey).first()
            }
            .toObservable()
            .doOnNext {
                Log.d("WeatherRepository", "Dispatching daily weather for city $cityKey from API")
            }
    }

    fun getNextDaysWeathers(cityKey: String): Observable<List<Weather>> {
        return Observable.concatArray(
            getNextDaysWeathersFromDb(cityKey),
            getNextDaysWeathersFromApi(cityKey)
        )
    }

    private fun getNextDaysWeathersFromDb(cityKey: String): Observable<List<Weather>> {
        return weatherDao.getNextDaysWeathers(cityKey)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext(Maybe.empty())
            .toObservable()
            .doOnNext {
                Log.d("WeatherRepository", "Dispatching Weather with id: $cityKey from DB...")
            }
    }

    private fun getNextDaysWeathersFromApi(cityKey: String): Observable<List<Weather>> {
        return weatherApiService.getWeatherForNextDays(cityKey)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .map { weatherResponse ->
                mapResponseToWeather(weatherResponse, cityKey).filter { it.date.isAfter(ZonedDateTime.now()) }
            }
            .toObservable()
            .doOnNext {
                Log.d("WeatherRepository", "Dispatching ${it.size} next days weathers from API")
            }
    }

    fun storeWeatherInDb(weather: Weather): Completable {
        return Completable.fromCallable { weatherDao.insertWeather(weather) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
            .doOnComplete {
                Log.d("WeatherRepository", "Inserted weather for city ${weather.cityKey} to DB")
            }
    }

    fun storeWeathersInDb(weathers: List<Weather>): Completable {
        return Completable.fromCallable { weatherDao.insertWeathers(weathers) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Completable.error(it) }
            .doOnComplete {
                Log.d("WeatherRepository", "Inserted ${weathers.size} weathers to DB")
            }
    }

    private fun mapResponseToWeather(
        weatherResponse: WeatherResponse,
        cityKey: String
    ): List<Weather> {
        return weatherResponse.DailyForecasts.map { dailyForecast ->
            Weather(
                date = ZonedDateTime.parse(dailyForecast.Date),
                temperatureMin = dailyForecast.Temperature.Minimum.getCelsiusValue(),
                temperatureMax = dailyForecast.Temperature.Maximum.getCelsiusValue(),
                cityKey = cityKey,
                description = weatherResponse.Headline.Text,
                category = weatherResponse.Headline.Category,
                dayIconUrl = ICONS_URL + String.format("%02d", dailyForecast.Day.Icon) + "-s" + ".png",
                dayDescription = dailyForecast.Day.IconPhrase,
                nightIconUrl = ICONS_URL + String.format("%02d", dailyForecast.Night.Icon) + "-s" + ".png",
                nightDescription = dailyForecast.Night.IconPhrase
            ).apply {
                val precipitationDay = dailyForecast.Day.PrecipitationIntensity?:"None"
                val precipitationNight = dailyForecast.Night.PrecipitationIntensity?:"None"
                dailyForecast.Day.PrecipitationType?.let {
                    when (it) {
                        RAIN -> this.dayRainIntensity = precipitationDay
                        SNOW -> this.daySnowIntensity = precipitationDay
                        ICE -> this.dayIceIntensity = precipitationDay
                        MIXED -> {
                            this.daySnowIntensity = precipitationDay
                            this.dayRainIntensity = precipitationDay
                        }
                    }
                }
                dailyForecast.Night.PrecipitationType?.let {
                    when (it) {
                        RAIN -> this.nightRainIntensity = precipitationNight
                        SNOW -> this.nightSnowIntensity = precipitationNight
                        ICE -> this.nightIceIntensity = precipitationNight
                        MIXED -> {
                            this.nightSnowIntensity = precipitationNight
                            this.nightRainIntensity = precipitationNight
                        }
                    }
                }
            }
        }
    }

}