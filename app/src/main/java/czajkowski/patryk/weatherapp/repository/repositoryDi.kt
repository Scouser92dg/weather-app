package czajkowski.patryk.weatherapp.repository

import org.koin.dsl.module

val repositoryModule = module {
    single { WeatherRepository(get(), get()) }
    single { CityRepository(get(), get()) }
}