package czajkowski.patryk.weatherapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class City(
        @PrimaryKey(autoGenerate = false)
        @SerializedName("cityKey") var cityKey: String,
        @SerializedName("localizedName") var localizedName: String,
        @SerializedName("englishName") var englishName: String,
        @SerializedName("countryLocalizedName") var countryLocalizedName: String,
        @SerializedName("countryEnglishName") var countryEnglishName: String
)