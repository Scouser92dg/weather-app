package czajkowski.patryk.weatherapp.model

data class CityResponse(
    var LocalizedName: String,
    var EnglishName: String,
    var Key: String,
    var Country: Country
)

data class Country(
    var Id: String,
    var LocalizedName: String,
    var EnglishName: String
)