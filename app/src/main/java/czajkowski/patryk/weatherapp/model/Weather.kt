package czajkowski.patryk.weatherapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import org.threeten.bp.ZonedDateTime

@Entity
data class Weather (
    @PrimaryKey(autoGenerate = false)
    @SerializedName("cityKey") var cityKey: String,
    @SerializedName("date") var date: ZonedDateTime,
    @SerializedName("temperatureMin") var temperatureMin: Double?,
    @SerializedName("temperatureMax") var temperatureMax: Double?,
    @SerializedName("dayRainIntensity") var dayRainIntensity: String = "None",
    @SerializedName("daySnowIntensity") var daySnowIntensity: String = "None",
    @SerializedName("dayIceIntensity") var dayIceIntensity: String = "None",
    @SerializedName("dayIconUrl") var dayIconUrl: String,
    @SerializedName("dayDescription") var dayDescription: String,
    @SerializedName("nightIconUrl") var nightIconUrl: String,
    @SerializedName("nightDescription") var nightDescription: String,
    @SerializedName("nightRainIntensity") var nightRainIntensity: String = "None",
    @SerializedName("nightSnowIntensity") var nightSnowIntensity: String = "None",
    @SerializedName("nightIceIntensity") var nightIceIntensity: String = "None",
    @SerializedName("description") var description: String,
    @SerializedName("category") var category: String
)