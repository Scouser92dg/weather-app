package czajkowski.patryk.weatherapp.model


data class WeatherResponse (
    var Headline: Headline,
    var DailyForecasts: List<DailyForecast>
)

data class Headline(
    var Text: String,
    var Category: String
)

data class DailyForecast(
    var Date: String,
    var Temperature: TemperatureMinMax,
    var Day: DayNight,
    var Night: DayNight
)

data class DayNight (
    var Icon: Int,
    var IconPhrase: String,
    var PrecipitationType: String?,
    var PrecipitationIntensity: String?
)

data class TemperatureMinMax(
    var Minimum: Temperature,
    var Maximum: Temperature
)

data class Temperature(
    var Value: Double?,
    var Unit: String,
    var UnitType: Int
)