package czajkowski.patryk.weatherapp.api

import czajkowski.patryk.weatherapp.model.*
import io.reactivex.Single

class StubWeatherApiService: WeatherApiService {

    val city = CityResponse( "Dąbrowa Górnicza", "Dabrowa Gornicza", "12332", Country("12", "Polska", "Poland"))
    val weather = WeatherResponse(
        Headline = Headline(
            "Sunny and thunderstorm sometimes",
            "Sunny"
        ),
        DailyForecasts = listOf(
            DailyForecast(
                "2020-08-15T10:15:30+02:00",
                TemperatureMinMax(
                    Temperature(20.3, "C", 1),
                    Temperature(30.4, "C", 1)
                ),
                DayNight(
                    23, "Sunny", "Snow", "Heavy"
                ),
                DayNight(
                    12, "Rain", "Rain", "Light"
                )
            )
        )
    )

    val weatherNextDays = WeatherResponse(
        Headline = Headline(
            "Sunny and thunderstorm sometimes",
            "Sunny"
        ),
        DailyForecasts = listOf(
            DailyForecast(
                "2020-08-15T10:15:30+02:00",
                TemperatureMinMax(
                    Temperature(20.3, "C", 1),
                    Temperature(30.4, "C", 1)
                ),
                DayNight(
                    23, "Sunny", "Snow", "Heavy"
                ),
                DayNight(
                    12, "Rain", "Rain", "Light"
                )
            ),
            DailyForecast(
                "2020-08-15T10:15:30+02:00",
                TemperatureMinMax(
                    Temperature(20.3, "C", 1),
                    Temperature(30.4, "C", 1)
                ),
                DayNight(
                    23, "Sunny", "Snow", "Heavy"
                ),
                DayNight(
                    12, "Rain", "Rain", "Light"
                )
            ),
            DailyForecast(
                "2020-08-15T10:15:30+02:00",
                TemperatureMinMax(
                    Temperature(20.3, "C", 1),
                    Temperature(30.4, "C", 1)
                ),
                DayNight(
                    23, "Sunny", "Snow", "Heavy"
                ),
                DayNight(
                    12, "Rain", "Rain", "Light"
                )
            ),
            DailyForecast(
                "2020-08-15T10:15:30+02:00",
                TemperatureMinMax(
                    Temperature(20.3, "C", 1),
                    Temperature(30.4, "C", 1)
                ),
                DayNight(
                    23, "Sunny", "Snow", "Heavy"
                ),
                DayNight(
                    12, "Rain", "Rain", "Light"
                )
            )
        )
    )

    override fun getCities(text: String, apiKey: String?, language: String?): Single<List<CityResponse>> {
        return Single.just(listOf(city))
    }

    override fun getCity(cityKey: String, apiKey: String?): Single<CityResponse> {
        return Single.just(city)
    }

    override fun getWeatherDaily(cityKey: String, apiKey: String?): Single<WeatherResponse> {
        return Single.just(weather)
    }

    override fun getWeatherForNextDays(cityKey: String, apiKey: String?): Single<WeatherResponse> {
        return Single.just(weatherNextDays)
    }

}