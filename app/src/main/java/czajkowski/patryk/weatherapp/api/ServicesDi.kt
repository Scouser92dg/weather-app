package czajkowski.patryk.weatherapp.api

import czajkowski.patryk.weatherapp.api.factory.GsonFactory
import czajkowski.patryk.weatherapp.api.factory.OkHttpClientFactory
import czajkowski.patryk.weatherapp.api.factory.RetrofitFactory
import org.koin.dsl.module
import retrofit2.Retrofit


val serviceModule = module {

    single {
        GsonFactory.createGson()
    }

    single {
        OkHttpClientFactory.createOkHttpClient()
    }

    single {
        RetrofitFactory.createRetrofit(get(), get())
    }

    single<WeatherApiService> { get<Retrofit>().create(WeatherApiService::class.java) }
//    single<WeatherApiService> { StubWeatherApiService() }

}
