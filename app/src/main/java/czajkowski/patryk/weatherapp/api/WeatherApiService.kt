package czajkowski.patryk.weatherapp.api

import czajkowski.patryk.weatherapp.WeatherApp
import czajkowski.patryk.weatherapp.model.CityResponse
import czajkowski.patryk.weatherapp.model.WeatherResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface WeatherApiService {

    @GET("locations/v1/cities/search")
    fun getCities(
        @Query ("q") text: String,
        @Query ("apikey") apiKey: String? = WeatherApp.ACCUWEATHER_API_KEY,
        @Query ("language") language: String? = "en-us"
    ): Single<List<CityResponse>>

    @GET("locations/v1/{cityKey}")
    fun getCity(
        @Path("cityKey") cityKey: String,
        @Query("apikey") apiKey: String? = WeatherApp.ACCUWEATHER_API_KEY
    ): Single<CityResponse>

    @GET("forecasts/v1/daily/1day/{cityKey}")
    fun getWeatherDaily(
        @Path ("cityKey") cityKey: String,
        @Query("apikey") apiKey: String? = WeatherApp.ACCUWEATHER_API_KEY
    ): Single<WeatherResponse>

    @GET("forecasts/v1/daily/5day/{cityKey}")
    fun getWeatherForNextDays(
        @Path("cityKey") cityKey: String,
        @Query("apikey") apiKey: String? = WeatherApp.ACCUWEATHER_API_KEY
    ): Single<WeatherResponse>

}