package czajkowski.patryk.weatherapp

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import czajkowski.patryk.weatherapp.api.serviceModule
import czajkowski.patryk.weatherapp.db.daoModule
import czajkowski.patryk.weatherapp.repository.repositoryModule
import czajkowski.patryk.weatherapp.ui.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WeatherApp : Application() {

    companion object {
        const val ACCUWEATHER_API_KEY = "yZz3uWwdQWKoALYOwG7SxkN0WaRoTkWy"
    }

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        startKoin {
            androidContext(this@WeatherApp)
            modules(
                listOf(
                    serviceModule,
                    viewModelModule,
                    repositoryModule,
                    daoModule
                )
            )
        }
    }
}