package czajkowski.patryk.weatherapp.util

import android.content.Context
import android.widget.TextView
import androidx.core.content.ContextCompat
import czajkowski.patryk.weatherapp.R

fun TextView.setColorByTemperature(context: Context, temperatureValue: Double) {
    val color =  when {
        temperatureValue < 10.0  -> R.color.md_blue_500
        temperatureValue >= 10.0 && temperatureValue < 30.0 -> R.color.md_black_1000
        else -> R.color.md_red_600
    }
    this.setTextColor(ContextCompat.getColor(context, color))
}