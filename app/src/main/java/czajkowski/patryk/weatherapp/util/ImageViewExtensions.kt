package czajkowski.patryk.weatherapp.util

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import czajkowski.patryk.weatherapp.R

fun ImageView.setImageByUrl(context: Context, iconUrl: String) {
    Glide.with(context)
        .load(iconUrl)
        .optionalCenterInside()
        .placeholder(R.drawable.cloudy)
        .into(this)
}