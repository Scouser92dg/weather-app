package czajkowski.patryk.weatherapp.util

import czajkowski.patryk.weatherapp.model.Temperature

fun Temperature.getCelsiusValue(): Double? {
    return if (this.Unit == "F") {
        this.Value?.fahrenheitToCelsius()
    } else if (this.Unit == "K") {
        this.Value?.kalvinToCelsius()
    } else {
        this.Value
    }
}