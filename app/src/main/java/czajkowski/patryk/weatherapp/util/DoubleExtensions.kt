package czajkowski.patryk.weatherapp.util

fun Double.fahrenheitToCelsius(): Double {
    return ((this - 32) / 1.8)
}

fun Double.kalvinToCelsius(): Double {
    return this - 273.15
}

