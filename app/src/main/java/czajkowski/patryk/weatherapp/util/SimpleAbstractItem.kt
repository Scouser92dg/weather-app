package czajkowski.patryk.weatherapp.util

import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem

abstract class SimpleAbstractItem : AbstractItem<SimpleAbstractItem.ViewHolder>() {

    override val type: Int = layoutRes

    override fun getViewHolder(v: View) =
        ViewHolder(v)

    abstract fun bind(view: View)

    class ViewHolder(itemView: View) : FastAdapter.ViewHolder<SimpleAbstractItem>(itemView) {

        override fun unbindView(item: SimpleAbstractItem) {
        }

        override fun bindView(item: SimpleAbstractItem, payloads: List<Any>) {
            item.bind(itemView)
        }
    }

}