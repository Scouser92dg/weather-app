package czajkowski.patryk.weatherapp

import czajkowski.patryk.weatherapp.ui.details.DetailsFragmentTest
import czajkowski.patryk.weatherapp.ui.search.SearchFragmentTest
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    SearchFragmentTest::class,
    DetailsFragmentTest::class
)
class TestsRunner {
}
