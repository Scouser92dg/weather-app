package czajkowski.patryk.weatherapp.ui.search

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import czajkowski.patryk.weatherapp.R
import czajkowski.patryk.weatherapp.ui.BaseFragmentTest
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters


@RunWith(AndroidJUnit4::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@LargeTest
class SearchFragmentTest: BaseFragmentTest() {

    @Before
    fun init() {
        waitUntilViewIsDisplayed(withId(R.id.searchFragmentLayout))
    }

    @Test
    fun checkIfSearchWorks() {
        onView(withMenuIdOrText(R.id.searchMenuItem, R.string.menu_search_label))
            .perform(ViewActions.click())

        onView(withHint(activityRule.activity.getString(R.string.search_hint)))
            .perform(ViewActions.typeText("Du"))

        waitUntilViewIsDisplayed(withId(R.id.citiesRecycler))

        Thread.sleep(2000)

        onView(withId(R.id.citiesRecycler))
            .check(matches(atPosition(0, withChild(withText("Du")))))
    }

    @Test
    fun checkIfSwitchToDetailsOnListItemClick() {
        onView(withMenuIdOrText(R.id.searchMenuItem, R.string.menu_search_label))
            .perform(ViewActions.click())

        onView(withHint(activityRule.activity.getString(R.string.search_hint)))
            .perform(ViewActions.typeText("Du"))

        waitUntilViewIsDisplayed(withId(R.id.citiesRecycler))

        Thread.sleep(2000)

        onView(withId(R.id.citiesRecycler))
            .perform(
                RecyclerViewActions
                .actionOnItemAtPosition<RecyclerView.ViewHolder>(0, clickOnViewChild(R.id.cityListItemNameText)))

        onView(withId(R.id.detailsFragmentLayout))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkLastCityItemAdded() {
        onView(withId(R.id.citiesRecycler))
            .check(matches(atPosition(0, withChild(withText("Du")))))
    }

    @Test
    fun checkLastCitiesItemClickSwitchToDetails() {
        onView(withId(R.id.citiesRecycler))
            .perform(
                RecyclerViewActions
                    .actionOnItemAtPosition<RecyclerView.ViewHolder>(0, clickOnViewChild(R.id.cityListItemNameText)))

        onView(withId(R.id.detailsFragmentLayout))
            .check(matches(isDisplayed()))
    }


}