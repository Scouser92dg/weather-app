package czajkowski.patryk.weatherapp.ui.details

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import czajkowski.patryk.weatherapp.R
import czajkowski.patryk.weatherapp.ui.BaseFragmentTest
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class DetailsFragmentTest: BaseFragmentTest() {

    @Before
    fun init() {
        goToDetails()
        waitUntilViewIsDisplayed(withId(R.id.detailsFragmentLayout))
        Thread.sleep(2000)
    }

    private fun goToDetails() {
        waitUntilViewIsDisplayed(withId(R.id.citiesRecycler))

        Espresso.onView(withId(R.id.citiesRecycler))
            .perform(
                RecyclerViewActions
                    .actionOnItemAtPosition<RecyclerView.ViewHolder>(0, clickOnViewChild(R.id.cityListItemNameText)))

        Espresso.onView(withId(R.id.detailsFragmentLayout))
            .check(matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun checkIfAllViewsFilled() {
        Espresso.onView(withId(R.id.detailsCityName))
            .check(matches(Matchers.not(ViewMatchers.withText(""))))
        Espresso.onView(withId(R.id.detailsDayIceValue))
            .check(matches(Matchers.not(ViewMatchers.withText(""))))
        Espresso.onView(withId(R.id.detailsDayRainValue))
            .check(matches(Matchers.not(ViewMatchers.withText(""))))
        Espresso.onView(withId(R.id.detailsDaySnowValue))
            .check(matches(Matchers.not(ViewMatchers.withText(""))))
        Espresso.onView(withId(R.id.detailsTemperatureMinText))
            .check(matches(Matchers.not(ViewMatchers.withText(""))))
        Espresso.onView(withId(R.id.detailsTemperatureMaxText))
            .check(matches(Matchers.not(ViewMatchers.withText(""))))
        Espresso.onView(withId(R.id.detailsNightIceValue))
            .check(matches(Matchers.not(ViewMatchers.withText(""))))
        Espresso.onView(withId(R.id.detailsNightRainValue))
            .check(matches(Matchers.not(ViewMatchers.withText(""))))
        Espresso.onView(withId(R.id.detailsNightSnowValue))
            .check(matches(Matchers.not(ViewMatchers.withText(""))))
    }

    @Test
    fun checkIfNextDaysListHas4Elements() {
        Espresso.onView(withId(R.id.detailsWeatherNextDaysRecycler)).check(matches((hasItemCount(4))))
    }


}