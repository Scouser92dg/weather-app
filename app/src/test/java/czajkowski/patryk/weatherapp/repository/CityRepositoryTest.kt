package czajkowski.patryk.weatherapp.repository

import android.accounts.NetworkErrorException
import com.nhaarman.mockitokotlin2.*
import czajkowski.patryk.weatherapp.BaseTest
import czajkowski.patryk.weatherapp.api.StubWeatherApiService
import czajkowski.patryk.weatherapp.api.WeatherApiService
import czajkowski.patryk.weatherapp.db.CityDao
import czajkowski.patryk.weatherapp.model.City
import io.reactivex.Maybe
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.io.IOException

class CityRepositoryTest  : BaseTest() {
    lateinit var repository: CityRepository

    private val dao: CityDao = mock()
    private val service: WeatherApiService = mock()

    private val city = City(
        cityKey = "12332",
        localizedName = "Dąbrowa Górnicza",
        englishName = "Dabrowa Gornicza",
        countryLocalizedName = "Polska",
        countryEnglishName = "Poland"
    )

    @Before
    fun init() {
        repository = CityRepository(service, dao)
    }

    @Test
    fun `should get city when api and db works fine`() {
        whenever(service.getCity(any(), any())).thenReturn(StubWeatherApiService().getCity("12332"))
        whenever(dao.getCityByKey("12332")).thenReturn(Maybe.just(city))

        repository.getCity("12332")
            .test()
            .assertValueAt(0, city)
            .assertValueAt(1, city)

        verify(service, atLeast(1)).getCity(any(), any())
        verify(dao, atLeast(1)).getCityByKey(any())
    }

    @Test
    fun `should get cities from api when api contains some cities`() {
        whenever(service.getCities(any(), any(), any())).thenReturn(StubWeatherApiService().getCities("Dąbrowa Górnicza"))

        repository.searchCities("Dąbrowa Górnicza")
            .test()
            .assertNoErrors()
            .assertValueAt(0, listOf(city))

        verify(service, atLeast(1)).getCities(any(), any(), any())
    }

    @Test
    fun `should not get cities from api and return error when api throw error`() {
        whenever(service.getCities(any(), any(), any())).thenReturn(Single.error(NetworkErrorException("500")))

        repository.searchCities("Dąbrowa Górnicza")
            .test()
            .assertError(NetworkErrorException::class.java)
    }

    @Test
    fun `should get cities from db when db contains some cities`() {
        whenever(dao.getAllCities()).thenReturn(Single.just(listOf(city)))

        repository.getCitiesFromDb()
            .test()
            .assertNoErrors()
            .assertValueAt(0, listOf(city))

        verify(dao).getAllCities()
    }

    @Test
    fun `should not get cities from db and return error when db throw error`() {
        whenever(dao.getAllCities()).thenReturn(Single.error(IOException()))

        repository.getCitiesFromDb()
            .test()
            .assertError(IOException::class.java)
    }

}