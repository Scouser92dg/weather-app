package czajkowski.patryk.weatherapp.repository

import android.accounts.NetworkErrorException
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import czajkowski.patryk.weatherapp.BaseTest
import czajkowski.patryk.weatherapp.api.StubWeatherApiService
import czajkowski.patryk.weatherapp.api.WeatherApiService
import czajkowski.patryk.weatherapp.db.WeatherDao
import czajkowski.patryk.weatherapp.model.Weather
import io.reactivex.Maybe
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.threeten.bp.ZonedDateTime


class WeatherRepositoryTest  : BaseTest() {
    lateinit var repository: WeatherRepository

    private val dao: WeatherDao = mock()
    private val service: WeatherApiService = mock()

    private val weather = Weather(
        cityKey = "12332",
        date = ZonedDateTime.parse("2020-08-15T10:15:30+02:00"),
        temperatureMin = 20.3,
        temperatureMax = 30.4,
        dayRainIntensity = "None",
        daySnowIntensity = "Heavy",
        dayIceIntensity = "None",
        dayIconUrl = "https://apidev.accuweather.com/developers/Media/Default/WeatherIcons/23-s.png",
        dayDescription = "Sunny",
        nightIconUrl = "https://apidev.accuweather.com/developers/Media/Default/WeatherIcons/12-s.png",
        nightDescription = "Rain",
        nightRainIntensity = "Light",
        nightSnowIntensity = "None",
        nightIceIntensity = "None",
        description = "Sunny and thunderstorm sometimes",
        category = "Sunny"
    )
    private val weatherNextDays = listOf(weather, weather, weather, weather)

    @Before
    fun init() {
        repository = WeatherRepository(service, dao)
    }

    @Test
    fun `should get weather daily when api and dao contains some weathers`() {
        whenever(service.getWeatherDaily(any(), any())).thenReturn(Single.just(StubWeatherApiService().weather))
        whenever(dao.getWeatherByCityKey(any())).thenReturn(Maybe.just(weather))

        repository.getDailyWeather("12332")
            .test()
            .assertNoErrors()
            .assertValueAt(0, weather)
            .assertValueAt(1, weather)

        verify(service).getWeatherDaily(any(), any())
        verify(dao).getWeatherByCityKey(any())
    }

    @Test
    fun `should not get weather daily weathers and return error when api throw error`() {
        whenever(dao.getWeatherByCityKey(any())).thenReturn(Maybe.just(weather))
        whenever(service.getWeatherDaily(any(), any())).thenReturn(Single.error(NetworkErrorException("500")))

        repository.getDailyWeather("12332")
            .test()
            .assertError(NetworkErrorException::class.java)
    }

    @Test
    fun `should get next days weathers when api and dao contains some weathers`() {
        whenever(service.getWeatherForNextDays(any(), any())).thenReturn(StubWeatherApiService().getWeatherForNextDays("12332"))
        whenever(dao.getNextDaysWeathers(any(),any(), any())).thenReturn(Maybe.just(weatherNextDays))

        repository.getNextDaysWeathers("12332")
            .test()
            .assertNoErrors()
            .assertValueAt(0, weatherNextDays)

        verify(service).getWeatherForNextDays(any(), any())
        verify(dao).getNextDaysWeathers(any(), any(), any())
    }

    @Test
    fun `should not get next days weathers and return error when api throw error`() {
        whenever(dao.getNextDaysWeathers(any(), any(), any())).thenReturn(Maybe.just(weatherNextDays))
        whenever(service.getWeatherForNextDays(any(), any())).thenReturn(Single.error(NetworkErrorException("500")))

        repository.getNextDaysWeathers("12332")
            .test()
            .assertError(NetworkErrorException::class.java)
    }

}