package czajkowski.patryk.weatherapp.ui.details

import android.accounts.NetworkErrorException
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import czajkowski.patryk.weatherapp.ui.BaseViewModelTest
import czajkowski.patryk.weatherapp.ui.model.CityModelWrapper
import czajkowski.patryk.weatherapp.ui.model.WeatherModelWrapper
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test

class DetailsViewModelTest : BaseViewModelTest() {

    lateinit var detailsViewModel: DetailsViewModel

    @Before
    override fun init() {
        super.init()

        detailsViewModel = DetailsViewModel(weatherRepository, cityRepository)
    }

    @Test
    fun `should fetch details when api and dao works fine`() {
        detailsViewModel.fetchDetails("12432")

        detailsViewModel.weatherToday.value?.let {
            assert(it == WeatherModelWrapper(weatherToday))
        }
        detailsViewModel.weathersNextDays.value?.let {
            assert(it == weatherNextDays)
        }
        detailsViewModel.city.value?.let {
            assert(it == CityModelWrapper(city))
        }
    }

    @Test
    fun `should show error message when some problems during fetch daily weather`() {
        whenever(weatherRepository.getDailyWeather(any())).thenReturn(
            Observable.error(
                NetworkErrorException()
            )
        )

        detailsViewModel.fetchDetails("12432")

        detailsViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

    @Test
    fun `should show error message when some problems during fetch next days weather`() {
        whenever(weatherRepository.getNextDaysWeathers(any())).thenReturn(
            Observable.error(
                NetworkErrorException()
            )
        )

        detailsViewModel.fetchDetails("12432")

        detailsViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

    @Test
    fun `should show error message when some problems during fetch city`() {
        whenever(cityRepository.getCity(any())).thenReturn(
            Observable.error(
                NetworkErrorException()
            )
        )

        detailsViewModel.fetchDetails("12432")

        detailsViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }


}