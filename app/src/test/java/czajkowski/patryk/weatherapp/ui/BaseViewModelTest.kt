package czajkowski.patryk.weatherapp.ui

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import czajkowski.patryk.weatherapp.BaseTest
import czajkowski.patryk.weatherapp.model.City
import czajkowski.patryk.weatherapp.model.Weather
import czajkowski.patryk.weatherapp.repository.CityRepository
import czajkowski.patryk.weatherapp.repository.WeatherRepository
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.Before
import org.threeten.bp.ZonedDateTime

open class BaseViewModelTest : BaseTest() {
    protected open val cityRepository: CityRepository = mock()
    protected open val weatherRepository: WeatherRepository = mock()

    protected open val weatherToday = Weather(
        cityKey = "1",
        date = ZonedDateTime.now(),
        temperatureMin = 20.0,
        temperatureMax = 30.0,
        dayRainIntensity = "None",
        daySnowIntensity = "Heavy",
        dayIceIntensity = "None",
        dayIconUrl = "imageDay.png",
        dayDescription = "It's a sample day description",
        nightIconUrl = "imageNight.png",
        nightDescription = "It's a sample night description",
        nightRainIntensity = "Light",
        nightSnowIntensity = "None",
        nightIceIntensity = "None",
        description = "It's a sample description",
        category = "Sunny"
    )

    protected open val weatherNextDays = listOf(
        weatherToday, weatherToday, weatherToday, weatherToday, weatherToday
    )

    protected open val city = City(
        cityKey = "12432",
        localizedName = "Dąbrowa Górnicza",
        englishName = "Dabrowa Gornicza",
        countryLocalizedName = "Polska",
        countryEnglishName = "Poland"
    )

    protected open val savedCities = listOf(
        City(
            cityKey = "1L",
            localizedName = "Dąbrowa Górnicza",
            englishName = "Dabrowa Gornicza",
            countryLocalizedName = "Polska",
            countryEnglishName = "Poland"
        ), City(
            cityKey = "2L",
            localizedName = "Czeladź",
            englishName = "Czeladz",
            countryLocalizedName = "Polska",
            countryEnglishName = "Poland"
        )
    )

    @Before
    open fun init() {
        whenever(cityRepository.getCity(any())).thenReturn(Observable.just(city))
        whenever(cityRepository.searchCities(any())).thenReturn(Observable.just(listOf(city)))
        whenever(cityRepository.getCitiesFromDb()).thenReturn(Observable.just(savedCities))
        whenever(weatherRepository.getDailyWeather(any())).thenReturn(Observable.just(weatherToday))
        whenever(weatherRepository.getNextDaysWeathers(any())).thenReturn(
            Observable.just(
                weatherNextDays
            )
        )
        whenever(weatherRepository.storeWeatherInDb(any())).thenReturn(Completable.complete())
    }

}