package czajkowski.patryk.weatherapp.ui.search

import android.accounts.NetworkErrorException
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import czajkowski.patryk.weatherapp.ui.BaseViewModelTest
import czajkowski.patryk.weatherapp.ui.model.CityModelWrapper
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test

class SearchViewModelTest : BaseViewModelTest() {

    lateinit var searchViewModel: SearchViewModel

    @Before
    override fun init() {
        super.init()

        searchViewModel = SearchViewModel(cityRepository)
    }


    @Test
    fun `should load saved cities on init`() {
        searchViewModel.cities.value?.let {
            assert(it.containsAll(savedCities.map { CityModelWrapper(it) }))
        }
    }

    @Test
    fun `should show error message when some problems during fetch cities from API`() {
        whenever(cityRepository.searchCities(any())).thenReturn(
            Observable.error(
                NetworkErrorException()
            ))

        searchViewModel.searchCities("Dąbrowa")

        searchViewModel.errorMessage.value?.let {
            assert(it.isNotBlank())
        }
    }

    @Test
    fun `should fetch cities successfuly`() {
        searchViewModel.searchCities("Dąbrowa")

        searchViewModel.cities.value?.let {
            assert(it.containsAll(listOf(CityModelWrapper(city))))
        }
    }

    @Test
    fun `should not fetch cities when typed 1 char only`() {
        searchViewModel.searchCities("D")

        verify(cityRepository, never()).searchCities(any())
    }

}